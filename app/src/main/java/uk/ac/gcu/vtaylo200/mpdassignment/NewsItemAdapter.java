package uk.ac.gcu.vtaylo200.mpdassignment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.LinkedList;

public class NewsItemAdapter extends ArrayAdapter<RoadworkItem> {
    Context context;
    int resource;
    LinkedList<RoadworkItem> items = null;

    public NewsItemAdapter(Context context, int resource, LinkedList<RoadworkItem> items) {
        super(context, resource, items);
        this.context = context;
        this.resource = resource;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        RoadworkItem item = items.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        }

        TextView titleTextView = (TextView)convertView.findViewById(R.id.titleTV);
        TextView pubDateTextView = (TextView)convertView.findViewById(R.id.pubDateTV);
        TextView descTextView = (TextView)convertView.findViewById(R.id.descTV);

        titleTextView.setText(item.getTitle());
        pubDateTextView.setText(item.getPubDate());
        descTextView.setText(item.getDesc());

        return convertView;
    }
}
