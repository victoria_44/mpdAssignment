package uk.ac.gcu.vtaylo200.mpdassignment;

/**
 * Created by taylorvr on 23/03/18.
 */

public class RoadworkItem {

    private String title;
    private String desc;
    private String link;
    private String georss;
    private String author;
    private String comment;
    private String pubDate;

    public RoadworkItem() {
        title = "";
        desc = "";
        link = "";
        georss = "";
        author = "";
        comment = "";
        pubDate = "";

    }

    public RoadworkItem(String atitle, String adesc, String alink, String ageorss, String aauthor, String acomment, String apubDate) {
        title = atitle;
        desc = adesc;
        link = alink;
        georss = ageorss;
        author = aauthor;
        comment = acomment;
        pubDate = apubDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getGeorss() {
        return georss;
    }

    public void setGeorss(String georss) {
        this.georss = georss;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }


} // End of class

