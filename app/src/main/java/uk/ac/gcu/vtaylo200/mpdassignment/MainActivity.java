package uk.ac.gcu.vtaylo200.mpdassignment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ViewSwitcher;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private String url1="http://trafficscotland.org/rss/feeds/currentincidents.aspx";
    private String url2="http://trafficscotland.org/rss/feeds/plannedroadworks.aspx";
    private ListView list;
    private Button showIncidents, plannedWorks, searchButton;
    private String result = "";
    private ListView listView;
    private Button currentIncidents, clearButton, home;
    private ViewSwitcher viewSwitch;
    private ProgressBar loader;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showIncidents = (Button)findViewById(R.id.showIncidents);
        showIncidents.setOnClickListener(this);
        plannedWorks = (Button) findViewById(R.id.plannedWorks);

        plannedWorks.setOnClickListener(this);
        searchButton = (Button) findViewById(R.id.searchButton);
        currentIncidents = (Button) findViewById(R.id.currentButton);
        home = (Button) findViewById(R.id.home);
        viewSwitch = (ViewSwitcher) findViewById(R.id.viewSwitch);
        listView = (ListView)findViewById(R.id.listView);
        clearButton = (Button)findViewById(R.id.clear);
        loader = (ProgressBar)findViewById(R.id.spinner);
        loader.setVisibility(View.GONE);


        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
            }
        });

        currentIncidents.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Show Previous View
                viewSwitch.showPrevious();

            }
        });

        home.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                // Show Previous View
                viewSwitch.showNext();

            }
        });

    }

    private LinkedList<RoadworkItem> parseData(String dataToParse)
    {
        LinkedList <RoadworkItem> workList = null;
        RoadworkItem roadwork = new RoadworkItem();
        try
        {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xmlPP = factory.newPullParser();
            xmlPP.setInput( new StringReader( dataToParse ) );
            int eventType = xmlPP.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                // Found a start tag
                if(eventType == XmlPullParser.START_TAG) {
                    // Check which Tag we have
                    if (xmlPP.getName().equalsIgnoreCase("channel")) {
                        workList  = new LinkedList<RoadworkItem>();
                    }
                    else if (xmlPP.getName().equalsIgnoreCase("item")) {
                        Log.e("MyTag","Item Start Tag found");
                        roadwork = new RoadworkItem();
                    }
                    else if (xmlPP.getName().equalsIgnoreCase("title")) {
                        // Now just get the associated text
                        String temp = xmlPP.nextText();
                        // Do something with text
                        Log.e("MyTag","title is " + temp);
                        roadwork.setTitle(temp);
                    }
                    else if (xmlPP.getName().equalsIgnoreCase("description")) {
                            // Now just get the associated text
                            String temp = xmlPP.nextText();
                            // Do something with text
                            Log.e("MyTag","desc is " + temp);
                            roadwork.setDesc(temp);
                        }
                        else if (xmlPP.getName().equalsIgnoreCase("link")) {
                                // Now just get the associated text
                                String temp = xmlPP.nextText();
                                // Do something with text
                                Log.e("MyTag","link is " + temp);
                                roadwork.setLink(temp);
                            }
                            else if (xmlPP.getName().equalsIgnoreCase("georss:point")) {
                                    // Now just get the associated text
                                    String temp = xmlPP.nextText();
                                    // Do something with text
                                    Log.e("MyTag","geo is " + temp);
                                    roadwork.setGeorss(temp);
                                }
                                else if (xmlPP.getName().equalsIgnoreCase("author")) {
                                        // Now just get the associated text
                                        String temp = xmlPP.nextText();
                                        // Do something with text
                                        Log.e("MyTag","author is " + temp);
                                        roadwork.setAuthor(temp);
                                    }
                                    else if (xmlPP.getName().equalsIgnoreCase("comments")) {
                                            // Now just get the associated text
                                            String temp = xmlPP.nextText();
                                            // Do something with text
                                            Log.e("MyTag","comment is " + temp);
                                            roadwork.setComment(temp);
                                        }
                                        else if (xmlPP.getName().equalsIgnoreCase("pubDate")) {
                                                // Now just get the associated text
                                                String temp = xmlPP.nextText();
                                                // Do something with text
                                                Log.e("MyTag","date is " + temp);
                                                roadwork.setPubDate(temp);
                                            }
                }
                else if(eventType == XmlPullParser.END_TAG) {
                    if (xmlPP.getName().equalsIgnoreCase("item")) {
                        Log.e("MyTag","item is " + roadwork.toString());
                        workList.add(roadwork);
                    }
                    else if (xmlPP.getName().equalsIgnoreCase("channel")) {
                        int size;
                        size = workList.size();
                        Log.e("MyTag","channel size is " + size);
                    }
                }

                // Get the next event
                eventType = xmlPP.next();

            } // End of while

            return workList;
        }
        catch (XmlPullParserException ae1) {
            Log.e("MyTag","Parsing error" + ae1.toString());
        }
        catch (IOException ae1) {
            Log.e("MyTag","IO error during parsing");
        }

        Log.e("MyTag","End document");

        return workList;

    }


    @Override
    public void onClick(View view) {
        startProgress(view);
    }

    public void startProgress(View view) {
        // Run network access on a separate thread for each url;
        if (view.getId() == (R.id.showIncidents)){
            loader.setVisibility(View.VISIBLE);
            new Thread(new Task(url1)).start();
        } else if (view.getId() == (R.id.plannedWorks)){
            loader.setVisibility(View.VISIBLE);
            new Thread(new Task(url2)).start();
        }
    }

    public void clear(){

        listView.setAdapter(null);
 }

    class Task implements Runnable {
        private String url;

        public Task(String aurl)
        {
            url = aurl;
        }
        @Override
        public void run() {

            URL aurl;
            URLConnection yc;
            BufferedReader in = null;
            String inputLine = "";


            Log.e("MyTag","in run");

            try {
                Log.e("MyTag","in try");
                aurl = new URL(url);
                yc = aurl.openConnection();
                in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
                //
                // Throw away the first 2 header lines before parsing
                //
                while ((inputLine = in.readLine()) != null) {
                    result = result + inputLine;
                    Log.e("MyTag",inputLine);

                }
                in.close();
            }
            catch (IOException ae) {
                Log.e("MyTag", "ioexception");
            }

            // Parse XML data to list

            final LinkedList<RoadworkItem> workList;

            workList = parseData(result);

            if (workList != null) {
                Log.e("MyTag", "List is not null");
                for (Object o : workList) {
                    Log.e("MyTag", o.toString());
                    //listView.append(o.toString());
                }
            } else {
                Log.e("MyTag", "List is null");
            }


            // Run the code, and on run() instantiate the NewsItemAdapter and use it in listView

            MainActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    Log.d("UI thread", "I am the UI thread");

                    NewsItemAdapter adapter = new NewsItemAdapter(getApplicationContext(), R.layout.list_item, workList);

                    listView.setAdapter(adapter);
                    loader.setVisibility(View.VISIBLE);
                    if (listView != null) {
                        loader.setVisibility(View.GONE);
                    }

                }
            });
        }

    }
}
